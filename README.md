# Gitlab CI Template

This template is intended as a turnkey solution for full continuous integration/development via Gitlab with a Docker foundation.  
It supports both Gitlab SaaS (Software as a Service, i.e. `https://gitlab.com`) and self-hosted of any tier.  
It can be run as a single-user CI/CD system, or as part of a larger Devops infrastructure.  
When added as a project to a preconfigured Gitlab group, a Docker container is instantiated that runs all scripting actions related to CI/CD operations.
A parameterised include file is used to simplify repo-specific configuration, which can be extended to other repos while utilising the instantiated container.  
Agile deployments of multiple Docker containers in a unified CI/CD infrastructure can be enabled by copying the `.gitlab-ci.yml` file to different repos and modifying as necessary.

This container leverages the Gitlab CI/CD Runner infrastructure, allowing for automatic spin-up/spin-down of runners.  
Network routing is handled via a Traefik proxy, deployed via the file `admin-stack.yml`.  

### Security
There is none. There are some basic checks to make sure that dumb mistakes aren't made, but there is intentionally no 
security hardening in this template.  
It has been created as an academic design of an optimal CI/CD infrastructure, with security left up to the implementor.

### A note on Gitlab versions
This repo is designed to support both Gitlab.com and self-hosted instances of any tier. However, Gitlab 16.0 changed the way runner registration tokens work.  
Previously, each project and group had their own registration tokens for the entire project (starting with "GR"). However in 16.0+, group runners are registered separately and have their own separate tokens (starting with "glrt"). Therefore, having a separate dev and prod environment for the API runner in Gitlab <16.0 would require a separate project, which is currently not supported.  

## Quick Start
- Read the assumptions. Read them again
- If you're on a free Gitlab.com account, read the section [Free Tier Limitations](#saas-free-tier-project-limitations)
- Get everything prepared in the [required environment](#minimum-required-environment)
  - The entire project can be run on a free Gitlab.com account and a Docker VM
  - A group is always required, or the ability to register group runners is unavailable
  - Separate tokens can be combined into one, however this is insecure. `DEV_*` tokens can be set to `""` to use one token for all requests. 
- Go through the steps in the [basic implementation](#basic-implementation)
- To use this template in other repos, see the file .gitlab-ci.yml for reference


## Assumptions
1. You have full control over the Docker swarm(s) involved in this infrastructure
2. You have the informed consent of the Gitlab administrator (for SaaS see the ToS)
2. Every developer with access to this system is trusted (i.e. this template is most definitely not secure and relies on the security of the underlying platform)
3. Every stage must be executable either on the Docker swarm manager (with the Ash shell) or via Bash on an API-authenticated Docker container
4. All service ports must be exposed on the default network for proxying to work
5. All access via token to alternate repos must use the same API token

## SaaS Free Tier Project Limitations
- Project and group access tokens are not available on the free tier. Whenever group or project access tokens are mentioned, instead use a personal access token.

## Optimal Environment
- Self-hosted Gitlab instance at 16.0+
- Three Docker swarms with Gitlab runners:
  - Build server(s), runner tags `[docker, build]`
  - Staging server(s), runner tags `[docker, staging, push]`
  - Production server(s), runner tags `[docker, prod]`
- admin-stack.yml deployed on the Staging and Production server
- DNS infrastructure to forward Traefik domains to `*.<dev_subdomain>` to Staging
  - With support to point arbitrary domains at the prod Docker manager
- This repo cloned to a devops group:
  - Public repo (available to all internal requests)
  - CI/CD project variables scoped to the devops group
  - Auto-registered Gitlab executor running as a Docker stack on Production, runner tags `[bash, scripting]`
- A separate group for active development
  - API and registration tokens for projects in that group must be scoped to that group/project
- Runner registration tokens:  
  _Runner registration on Gitlab <16.0 is limited to a single scoped runner registration token. Use said token as the "API Runner" token and leave the dev token at default._
    ``` 
    "API Runner":
      - Scope: group
      - Tags: [bash, scripting]
    "Dev API Runner":
      - Scope: group
      - Tags: [devbash, scripting]
    ```
- API runner access token:
  - `API_runner_token`:
    ```
    - Scope: group 
    - Role: Maintainer
    - Access: [api, write_registry, read_repo]
    - Expiry: long
    ```

## Minimum Required Environment
This is the minimum requirement for [implementation](#implementation).
- A Gitlab.com account with a group
- A Docker swarm with root access
  - A single host is sufficient (`docker swarm init --force-new-cluster`)
  - If multiple nodes are present:
    - The manager(s) **must** be on static or reserved IPs
    - Shared volume storage should be configured between the nodes 
- A Gitlab runner running on a swarm manager as a service, with the following attributes:
  - Tags: `[docker, scripting, build, push, staging, prod, bash]`
  - Protected: no
  - Privileged: yes
  - Image: `docker:latest`
  - Volumes: `["/var/run/docker.sock:/var/run/docker.sock"]`
- Runner registration token:  
  - **Gitlab 16.0+:**  
    - Create a New Group Runner from the CI/CD -> Runners section in any given scope
    - Tags: `[bash, scripting]`    
  - **Gitlab <16.0:**
    - Obtain the runner registration token for your scope from the page [Settings ->] CI/CD -> Runners
- Group authentication token  
  ```
  - Role: Maintainer
  - Access: [api, write_registry, read_repo]
  ```

## Basic Implementation
1. Clone or fork the repo into the group
2. Cancel the pipeline that just started
3. Navigate to Settings -> CI/CD -> Variables and add the following (flags `Masked, !Protected, !Expanded`):  
    ``` 
    REGISTRATION_TOKEN:
      - Source: "Runner registration token"

    API_JOB_TOKEN:
      - Source: "Group authentication token"
    ```
4. Push an empty commit to the default branch (i.e. edit the README and add a new line at the end)
5. A new stack is deployed (on the Docker swarm tagged `prod`) named `gitlabci` with a service named `gitlabci-apirunner`

## Admin Stack

`docker stack deploy -c ./admin-stack.yml admin`  

Runs Traefik (a Docker reverse proxy) and Swarmpit (a Docker monitor)  
Configure your DNS to point `*.$TRAEFIK_DEV_DOMAIN.$TRAEFIK_ROOT_DOMAIN` at the Docker staging manager  
Add deploy labels to containers to add them to the proxy (see 'docker-compose-dev.yml')  
Recommended production deployment is static DNS entries for specific containers pointing at the Docker production manager 

## Troubleshooting
> My pipeline is stuck!  

Make sure that all required runner tags are covered by your available runners

> My first pipeline failed!

Is this the pipeline that you needed to cancel?  
Have you added the CI/CD variables?  
Is your Gitlab runner configured for Docker privileged mode?  
Are you getting token permission errors in the failing stage?
