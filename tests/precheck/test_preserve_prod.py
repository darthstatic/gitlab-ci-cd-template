import os


# Test template
# Copy into another file and replace as necessary
#
# class TestTemplate:
#
#     def setup_method(self):
#         self.test_variable = "TEST"
#
#     def test_1(self):
#         assert self.test_variable == "TEST"


class TestEnvVars:

    def setup_method(self):
        self.envvars = dict(os.environ)
        self.dev_env = {}
        self.keys = dict(os.environ).keys()
        for key in self.keys:
            if key.startswith('DEV_'):
                self.dev_env.update({key.split('DEV_', 1)[1]: os.environ.get(key)})
        # return self

    def test_no_default_keys(self):
        defaults = ['HOME', 'LANG', 'LANGUAGE', 'LOGNAME',
                    'MAIL', 'PATH', 'PWD', 'SHELL',
                    'SHLVL', 'TERM', 'USER']
        for key in self.dev_env.keys():
            # Assert that DEV_ is not trying to overwrite a default
            assert key not in defaults

    def test_no_prod_copy(self):
        for key in self.dev_env.keys():
            # Assert that DEV_ is not trying to copy prod
            assert self.dev_env[key] != self.envvars[key]

