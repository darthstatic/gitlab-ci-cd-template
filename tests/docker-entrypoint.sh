#!/bin/bash

tests=$1
shift
if [[ $UNITTESTS == "precheck" ]]; then
	pytest -q "/tests/precheck/*"
fi