FROM gitlab/gitlab-runner:latest


# Install the Docker CLI from the Docker repo
RUN apt update -qq 2>/dev/null
RUN apt install -qq -y gnupg1 python3-pytest-cov jq 2>/dev/null
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg1 --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
RUN  echo "deb [arch="$(dpkg --print-architecture)" signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
     https://download.docker.com/linux/ubuntu \
     "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
     tee /etc/apt/sources.list.d/docker.list
RUN apt update -qq
RUN apt install -qq -y docker-ce-cli

# Args are down here to put the docker install on a base image layer
ARG GITLAB_TOKEN
ARG GITLAB_TOKEN_KEY
ARG CUSTOM_CA_FILE_URL

# Example usage of REPO_READ_TOKEN
# RUN git clone https://oauth2:$REPO_READ_TOKEN@$CI_SERVER_HOST/<project>/<repo>.git <repo>/

COPY --chmod=777 entrypoint.sh /

# If custom CA file is provided, add it
RUN if [ -n "${CUSTOM_CA_FILE_URL}" ] ; then \
    mkdir -p /usr/local/share/ca-certificates ;\
    curl -ks "${CUSTOM_CA_FILE_URL}" >> /usr/local/share/ca-certificates/custom_certs.crt ;\
    update-ca-certificates ;\
    fi

ENV DATA_DIR="/etc/gitlab-runner"
ENV CONFIG_FILE="/etc/gitlab-runner/config.toml"
ENV CI_SERVER_TLS_CA_FILE="/etc/ssl/certs/ca-certificates.crt"

RUN touch /test9

STOPSIGNAL SIGQUIT
ENTRYPOINT ["/entrypoint.sh"]