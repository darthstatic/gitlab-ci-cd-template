#!/usr/bin/bash

_term() {
  # Send runner a QUIT signal
  kill -QUIT "$child" 2>/dev/null
  # ...and unregister the runner by name
  gitlab-runner unregister --name "$(hostname)"
}



# Trap exit signals
trap _term SIGTERM SIGQUIT

# Additional params for runners on pre-16.0 Gitlab
REGISTER_PARAMS=""
if [ $CI_SERVER_VERSION_MAJOR -lt 16 ]; then
  if [ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]; then
    REGISTER_PARAMS="--tag-list bash,scripting"
  else
    REGISTER_PARAMS="--tag-list bashdev,scripting --maintenance-note $CI_COMMIT_BRANCH"
  fi
fi

# Register runner using envvars
gitlab-runner register -n $REGISTER_PARAMS

# Run unit tests if defined, else start the runner
if [ "$UNITTESTS" == "TRUE" ]; then
  #TODO: add unit tests for runner
  /bin/true
  gitlab-runner unregister --name "$(hostname)"
else
  # Fork gitlab-runner passing all arguments
  gitlab-runner run &
  # Get gitlab-runner PID and wait for signal
  child=$!
  wait "$child"
fi


